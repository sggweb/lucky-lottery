'use strict'

var logger = require('./log').logger;
var config = require('./config').config;
var fs = require("fs");
var data = fs.readFileSync(__dirname + '/data/users.txt');
var allUsers = data.toString().trim().split("\n");
var current = null;
var tasks = [];
var rewards = [];

if (!Array.indexOf) {
    Array.prototype.indexOf = function (obj) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == obj) {
                return i;
            }
        }
        return -1;
    }
}
initData();

/**
 * 过滤排除人员，得到可抽奖的人员
 * @param {Object} except
 */
function filterRestUser(users,except) {
    var restUsers = [];
    for (var i = 0; i < users.length; i++) {
        var user = users[i].trim();
        if (except.indexOf(user) == -1) {
            restUsers.push(user);
        }
    }
    //logger.info('排除名单：' + except.toString());
    //logger.info('剩下名单：' + restUsers.toString());
    return restUsers;
}

/**
 * 初始化数据
 */
function initData() {
    for (var i = 0; i < config.tasks.length; i++) {
        var task = config.tasks[i];
        tasks.push({
            id: i,
            title: task.title,
            restUsers: filterRestUser(allUsers,task.except),   //过滤掉(全局)排除人员的剩余可用抽奖人员
            consumeUsers: [],   //该任务已经消费掉的人
            lastRandUsers: []   //本次活动消费掉的人
        });
        if (task.rewards) {
            //循环添加活动到rewards中
            for (var j=0; j<task.rewards.length; j++) {
                var reward = task.rewards[j];
                rewards.push({
                    id: j,
                    taskId: i,
                    title: reward.title,
                    count: reward.count,
                    capacity: reward.capacity,
                    fontSize: reward.nameFontSize,
                    consume: 0,
                    cols: getCols(reward.namesOfLine),
                    include: reward.include,   //本次抽奖必须包含人员
                    thisExcept: []   //本次活动排除的人员（后续活动必须包含的人员，不能在前序活动中消费掉）
                });
            }

            //设置活动排除的人员
            var thisExceptPerson=[];
            for (var j=rewards.length-1; j >=0 ; j--) {
                var reward = rewards[j];
                if(reward.taskId == i) { //同一个task的活动
                    //设置本次活动排除人员
                    for (var n=0; n<thisExceptPerson.length; n++) {
                        reward.thisExcept.push(thisExceptPerson[n]);
                    }
                    //添加包括人员到变量,下个循环使用
                    for (var m=0; m<reward.include.length; m++) {
                        thisExceptPerson.push(reward.include[m]);
                    }
                    //logger.info("reward.taskId"+reward.taskId+"  reward.title:"+reward.title+"/r/n reward.thisExcept："+reward.thisExcept.toString()+"/r/n reward.include:"+reward.include.toString());
                }
            }
        }
    }
}

function getCols(number) {
    switch (number) {
        case 1:
            return 12;
        case 2:
            return 6;
        case 3:
            return 4;
        case 4:
            return 3;
        case 6:
            return 2;
        case 12:
            return 1;
        default:
            return 4;
    }
}

/**
 * 判断是否可启动
 */
function canStart() {
    return !isRewardCompleted(current);
}

/**
 * 判断抽奖是否完成
 * @param {Object} reward
 */
function isRewardCompleted(reward) {
    if (reward != null) {
        return reward.count > reward.consume ? false : true;
    }
    return true;
}

/**
 * 获取下一个抽奖活动
 */
function nextReward() {
    if (isRewardCompleted(current)) {
        current = rewards.shift();
    }
    return current;
}

/**
 * 抽奖-获取本次抽奖人员
 */
function randomUsers() {
    if (!isRewardCompleted(current)) {
        var randomUsers = [];  //抽取的得奖人
        var task = tasks[current.taskId];
        var rewardUsers = filterRestUser(task.restUsers,current.thisExcept);  //排除掉后续必须得奖人员
        //上次剩余必须包含人员
        var mustUsers = [];
        for (var m = 0; m < current.include.length; m++) {

            if (rewardUsers.indexOf(current.include[m]) != -1) {
                mustUsers.push(current.include[m]);
            }
        }

        var rest = current.count - current.consume;
        var consumeNumber = rest < current.capacity ? rest : current.capacity;
        consumeNumber = rewardUsers.length < consumeNumber ? rewardUsers.length : consumeNumber;  //本次需要消耗的人数

        var mustConsumeNumber = mustUsers.length < consumeNumber ? mustUsers.length : consumeNumber;  //本次必须抽中的人数

        var thisMustlotteryNum = consumeNumber-mustConsumeNumber;    //本次必须抽奖人（可抽奖人中去除必须抽中的人）
        //本次可抽奖人
        var thisNeedLottery = [];
        var remainUsers = filterRestUser(rewardUsers,current.include);  //上次剩余本次可抽奖人（减去了所有必须包含的人）
        for(var p=0;p<remainUsers.length;p++)
        {
            if(p<thisMustlotteryNum)
            {
                thisNeedLottery.push(remainUsers[p]);
            }
            else
            {
                break;
            }
        }

        if(thisMustlotteryNum > 0)  //上次剩余可抽奖人-本次必须得奖人，数量>0时，进行抽奖
        {
            //循环抽取
            while (randomUsers.length < (thisMustlotteryNum)) {
                var idx = Math.floor(Math.random() * thisMustlotteryNum);
                if (randomUsers.indexOf(thisNeedLottery[idx]) == -1) {
                    randomUsers.push(thisNeedLottery[idx]);
                }
            }
        }
        else
        {
            //不抽取人，直接是本次必须人员
        }

        //添加本次必须抽中人员
        for (var n = 0; n < mustConsumeNumber; n++) {
            randomUsers.push(mustUsers[n]);
        }

        //已抽取的人员，重新全部数据抽奖100%，防止画面上人员位置不动
        var randomUsers2=[];
        while (randomUsers2.length < (randomUsers.length)) {
            var idx = Math.floor(Math.random() * randomUsers.length);
            if (randomUsers2.indexOf(randomUsers[idx]) == -1) {
                randomUsers2.push(randomUsers[idx]);
            }
        }

        tasks[current.taskId].lastRandUsers = randomUsers2;
        return randomUsers2;
    }

    return false;
}

/**
 * 抽奖-本次抽奖结束
 */
function completedOnceRolling() {
    if (!isRewardCompleted(current)) {
        var task = tasks[current.taskId];
        logger.info('Lottery taskId ' + current.taskId + '本次获奖随机用户: ' + task.lastRandUsers.toString());
        if (task.lastRandUsers.length > 0) {
            var rest = current.count - current.consume;
            var consumeNumber = rest < current.capacity ? rest : current.capacity;
            var restUsers = [];
            for (var i = 0; i < task.restUsers.length; i++) {
                if (task.lastRandUsers.indexOf(task.restUsers[i]) == -1) {
                    restUsers.push(task.restUsers[i]);
                }
            }
            current.consume = current.consume + consumeNumber;
            current.include = filterRestUser(current.include,task.lastRandUsers);  //删除本次已经中奖的人员
            tasks[current.taskId].restUsers = restUsers;
            tasks[current.taskId].lastRandUsers = [];
            tasks[current.taskId].consumeUsers.push(task.lastRandUsers);
            logger.info('Lottery taskId ' + current.taskId + '本次不可抽取用户（后续必须抽中人员）: ' + current.include.toString()+current.thisExcept.toString());
            logger.info('Lottery taskId ' + current.taskId + ' 本次抽奖余下用户:' + tasks[current.taskId].restUsers.toString());
            return true;
        }
    }
    return false;
}


module.exports = {
    config: config,
    currentReward: () => {
    return current;
},
canStart: canStart,
    nextReward: nextReward,
    randomUsers: randomUsers,
    completedOnceRolling: completedOnceRolling
}